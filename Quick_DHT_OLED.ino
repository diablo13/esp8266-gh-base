bool is_testing = false;

// wifi
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266HTTPClient.h>

//const char *ssid = "MiracleFarm_2.4G";  
//const char *password = "MadameLingzhi";
//const char *ssid = "Diablo";  
//const char *password = "123456788";
const char *ssid = "Disruptor_2.4G";  
const char *password = "Ong@0921";

//Temperature & RH% Sensor

#include <DHT.h>
#include <DHT_U.h>

#define DHT_PIN D5
#define DHT_TYPE_1 DHT21   // DHT 22  (AM2302)
#define DHT_TYPE_2 DHT22

DHT dht(DHT_PIN, DHT_TYPE_2);

//Carbon
#define CARBON_PIN D6

//OLED
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

//Common
const String box_name = "ct1";

bool is_connecting = false;
String wifi_dot = "";

int update_time_count = 0;
const int update_second = 6*3;
const int clock_time = 10000;
const int update_time = clock_time*update_second;


void setup() 
{

  if(!is_testing) {
    clock_time = update_time;
  }
  
  Serial.begin(115200); 
  dht.begin();

  if(is_testing) {
  
    // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
      Serial.println(F("SSD1306 allocation failed"));
      for(;;); // Don't proceed, loop forever
    }

    displayOLED("  Chateau  ", 2, 0, 0);
  
  }
  
  Serial.println("Setup done");  

  delay(2000);
  
  connectWifi();
  
}

void loop() {

  if (WiFi.status() != WL_CONNECTED)
  {
      connectWifi();
  }
  
  if(update_time_count >= update_time) {  

    float temperature = dht.readTemperature();
    float rh = dht.readHumidity();
    int carbon = analogRead(CARBON_PIN);
  
    String display_str = "TP  " + String(temperature) + "\nRH  " + String(rh) + "\nCO2 "+ String(carbon);
    displayOLED(display_str, 2, 0, 0);
    Serial.println("T " + String(temperature) + "\nRH " + String(rh) + "\nCarbon "+ String(carbon));
    
    //Send data to server
    updateBox(temperature, rh, carbon);
    update_time_count = update_time_count-update_time;    
  }
  
  delay(clock_time);
  
  update_time_count += clock_time;
  
}

void connectWifi(){

  is_connecting = true;
  
  Serial.println("connectWifi"); 
  
  WiFi.mode(WIFI_OFF);
  delay(1000);
  WiFi.mode(WIFI_STA);
  
  WiFi.begin(ssid, password);

  String display_string;
  display_string = "ConnectingWIFI";
  wifi_dot = "";
  displayOLED(display_string, 2, 0, 20);

  Serial.println(WiFi.status()) ; 
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    wifi_dot += ".";
    if(wifi_dot.length() > 4) {
      wifi_dot = "";
    }
    
    display_string = "ConnectingWIFI";
    display_string += wifi_dot;
    displayOLED(display_string, 2, 0, 20);
  }

  display_string = "Connected to ";
  display_string += ssid;
  display_string += "  IP ";
  //display_string += WiFi.localIP();
  displayOLED(display_string, 1, 0, 20);

  is_connecting = false;
    
  delay(1000);

}

void updateBox(float temperature, float rh, int carbon) {

  HTTPClient http;

  String postData = "";
  postData += + "[{";
  postData += "\"name\":\""+ box_name +"\"";
  postData += ",\"temperature\":\""+ String(temperature,2) +"\"";
  postData += ",\"relative_humidity\":\""+ String(rh,2) + "\"";
  postData += ",\"carbon\":\"" + String(carbon,2) + "\"";

  postData = postData + "}]";
  
  http.begin("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/update?data="+postData);
  Serial.println("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/update?data="+postData);
  
  int httpCode = http.GET(); 
  String payload = http.getString();  

  http.end();

  Serial.print("updateBox done ");
  Serial.println(httpCode);
  
}

void displayOLED(String display_str, int text_size, int x, int y) {

  if(!is_testing) return;
  
  display.clearDisplay();
  display.setTextSize(text_size); // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(x, y);
  display.println(display_str);
  display.display(); 

}
